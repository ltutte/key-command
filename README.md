# Key-command
## Description
The key-command or kc is a command line based software which can be used to encrypt, decrypt and hash strings.


# Project status
---
| Version | Supported | Comment | is usable |
| ------- | --------- | ------- | --------- |
| 0.0.1   | :x: | The program is currently in the preparation phase | :x: |
---
| Feature | Supported | Comment | is usable |
| ------- | --------- | ------- | --------- |
| UTF8 strings   | :x: |  | :x: |
| OpenSSL General   | :x: |  | :x: |
| OpenSSL Encryption  | :x: |  | :x: |
| OpenSSL Decryption   | :x: |  | :x: |